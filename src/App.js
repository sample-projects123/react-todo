import { useEffect, useState } from "react";
import { Spinner } from "react-bootstrap";
import TodoService from "./API/TodoService";
import styles from "./App.module.css";
import AddTodoForm from "./components/AddTodoForm/AddTodoForm";
import TodoList from "./components/TodoList/TodoList";
import { useFetching } from "./hooks/useFetching";

function App() {

    const [todoList, setTodoList] = useState([])
    const [fetchTodoList, isTodoListLoading, todoListError] = useFetching(async () => setTodoList(await TodoService.getAll()))

    useEffect(() => fetchTodoList(), [])

    return (
        <div className={styles.app}>
            <AddTodoForm addTodoCallback={newTodo => setTodoList([...todoList, newTodo])} />
            {
                isTodoListLoading
                    ? <div className={styles.loader}>
                        <Spinner animation="grow" variant="success" className="flex justify-content-center" />
                    </div>
                    : <TodoList
                        todoList={todoList}
                        title="Todo List"
                        todoListError={todoListError}
                        removeTodoCallback={todo => setTodoList(todoList.filter(t => t.id !== todo.id))}
                        editTodoCallback={todo => setTodoList(todoList.map(t => t.id === todo.id ? todo : t))}
                    />
            }
        </div>
    );
}

export default App;
