import { useMemo } from "react";

export const useTodoListStateFilter = (todoList, state) => {
    const filteredTodoList = useMemo(() => {
        switch (state) {
            case "2":
                return [...todoList].filter(todo => !todo.isComplete)
            case "3":
                return [...todoList].filter(todo => todo.isComplete)
            default:
                return todoList
        }
    }, [state, todoList])

    return filteredTodoList
}

export const useTodoListFilter = (todoList, state, query) => {
    const filteredTodoList = useTodoListStateFilter(todoList, state)

    const filteredAndSearchedTodoList = useMemo(() => {
        return filteredTodoList.filter(todo => todo.name.toLowerCase().includes(query.toLowerCase()))
    }, [query, filteredTodoList])

    return filteredAndSearchedTodoList
}
