import { useState } from "react";
import TodoItem from "../TodoItem/TodoItem";
import TodoFilter from "../TodoFilter/TodoFilter";
import { useTodoListFilter } from "../../hooks/useTodoListFilter";
import styles from "./TodoList.module.css";

function TodoList({ todoList, title, todoListError, removeTodoCallback, editTodoCallback }) {
    const [filter, setFilter] = useState({ state: "1", query: "" })
    const filteredAndSearchedTodoList = useTodoListFilter(todoList, filter.state, filter.query)

    return (
        <div className={styles.container}>
            <h1>{title}</h1>
            <TodoFilter filter={filter} setFilter={setFilter} />
            {
                todoListError && <h1>An error has occurred: ${todoListError}</h1>
            }
            <div>
                {
                    !filteredAndSearchedTodoList.length
                        ? <h1>Nothing to do...</h1>
                        : filteredAndSearchedTodoList.map(todo =>
                            <TodoItem key={todo.id}
                                todo={todo}
                                removeTodoCallback={removeTodoCallback}
                                editTodoCallback={editTodoCallback}
                            />
                        )
                }
            </div>
        </div>
    )
}

export default TodoList;
