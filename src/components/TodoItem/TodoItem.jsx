import { useState } from "react";
import { Form, ButtonGroup, Button, Row, Col } from "react-bootstrap";
import TodoService from "../../API/TodoService";
import EditTodoForm from "../EditTodoForm/EditTodoForm";
import styles from "./TodoItem.module.css";

function TodoItem({ todo, removeTodoCallback, editTodoCallback }) {
    const [isEditing, setIsEditing] = useState(false)

    async function handleChange(isComplete) {
        const editedTodo = {...todo, isComplete: isComplete}

        await TodoService.update(editedTodo)

        editTodoCallback(editedTodo)
    }
    
    function handleEdit() {
        setIsEditing(true)
    }

    async function handleRemove() {
        const deletedTodo = await TodoService.delete(todo.id)
        
        removeTodoCallback(deletedTodo)
    }

    return (
        <Row className={styles.todo}>
            <Col xs lg="1" className={"align-self-center " + styles.colCenterAlign}>
                <Form.Check
                    type="checkbox"
                    onChange={e => handleChange(e.target.checked)}
                    checked={todo.isComplete} />
            </Col>
            <Col className="align-self-center text-break">
                {
                    isEditing
                        ? <EditTodoForm todo={todo}
                            editTodoCallback={editTodoCallback} 
                            isEditingCallback={() => setIsEditing(false)} />
                        : todo.name
                }
            </Col>
            <Col xs lg="2" className={styles.colCenterAlignBtn}>
                <ButtonGroup>
                    <Button size="sm" onClick={handleEdit}>
                        Edit
                    </Button>
                    <Button size="sm" variant="danger" onClick={handleRemove}>
                        Remove
                    </Button>
                </ButtonGroup>
            </Col>
        </Row>
    )
}

export default TodoItem;
