import { useState } from "react";
import { Form, Button, InputGroup } from "react-bootstrap";
import TodoService from "../../API/TodoService";
import styles from "./AddTodoForm.module.css";

function AddTodoForm({addTodoCallback}) {

    const [todoName, setTodoName] = useState("")

    async function handleAddTodo() {
        const newTodo = {
            name: todoName,
            isComplete: false
        }

        const createdTodo = await TodoService.create(newTodo);
        
        addTodoCallback(createdTodo)
        setTodoName("")
    }

    return (
        <InputGroup className={styles.form}>
            <Form.Control
                placeholder="Enter todo"
                value={todoName}
                onChange={e => setTodoName(e.target.value)}
            />
            <Button
                variant="success"
                onClick={handleAddTodo}
            >
                Add
            </Button>
        </InputGroup>
    )
}

export default AddTodoForm;
