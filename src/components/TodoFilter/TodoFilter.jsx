import { Form } from "react-bootstrap";
import FilterButtonGroup from "../FilterButtonGroup/FilterButtonGroup";

function TodoFilter({filter, setFilter}) {
    return (
        <div>
            <Form.Control className="mb-1"
                placeholder="Enter search query"
                value={filter.query}
                onChange={e => setFilter({...filter, query: e.target.value})}
            />
            <FilterButtonGroup
                options={[
                    { name: "All", value: "1" },
                    { name: "Uncompleted", value: "2" },
                    { name: "Completed", value: "3" },
                ]}
                current={filter.state}
                changeFilterCallback={state => setFilter({...filter, state: state})}
            />
        </div>
    )
}

export default TodoFilter;
