import { useState } from "react";
import { Form, Button, InputGroup } from "react-bootstrap";
import TodoService from "../../API/TodoService";

function EditTodoForm({ todo, editTodoCallback, isEditingCallback }) {

    const [todoName, setTodoName] = useState(todo.name)

    async function handleClick() {
        const editedTodo = {...todo, name: todoName}

        await TodoService.update(editedTodo)

        editTodoCallback(editedTodo)
        isEditingCallback()
    }

    return (
        <InputGroup>
            <Form.Control size="sm"
                value={todoName}
                onChange={e => setTodoName(e.target.value)}
            />
            <Button size="sm"
                variant="success"
                onClick={handleClick}
            >
                Save
            </Button>
        </InputGroup>
    )
}

export default EditTodoForm;