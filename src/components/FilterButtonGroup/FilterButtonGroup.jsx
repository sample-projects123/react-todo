import { ButtonGroup, ToggleButton } from "react-bootstrap";

function FilterButtonGroup({ options, current, changeFilterCallback }) {

    return (
        <ButtonGroup>
            {options.map((radio, idx) => (
                <ToggleButton
                    key={idx}
                    id={`radio-${idx}`}
                    type="radio"
                    variant="outline-dark"
                    name="radio"
                    value={radio.value}
                    checked={current === radio.value}
                    onChange={(e) => changeFilterCallback(e.currentTarget.value)}
                >
                    {radio.name}
                </ToggleButton>
            ))}
        </ButtonGroup>
    )
}

export default FilterButtonGroup;
