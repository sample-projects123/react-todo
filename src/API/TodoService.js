import axios from "axios";

export default class TodoService {
    static async getAll() {
        const response = await axios.get("https://localhost:7079/todoitems")

        return response.data
    }

    static async create(todo) {
        const response = await axios.post("https://localhost:7079/todoitems", todo)
        
        return response.data
    }

    static async update(todo) {
        const response = await axios.put(`https://localhost:7079/todoitems/${todo.id}`, todo)
        
        return response.data
    }

    static async delete(id) {
        const response = await axios.delete(`https://localhost:7079/todoitems/${id}`)
        
        return response.data
    }
}
